""" getlocsdata.py

Drift correct and store data for all localisations over multiple palm3d
acquisitions for a FOV.

palm3d data may be contained in multiple acquisitions repeated for the
same FOV, if labels were still blinking with sufficient frequency to
justify extra runs.

For each repeat, there is a list of localisations and associated data: a
'localizations file'. There is also a .pkl file containing other data about the
acquisition, including drift.

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

import cPickle, shelve
from tkFileDialog import askopenfilename
import numpy as np
import pandas as pd

def getp3dfiles(loadfolder):
    """
    Get file locations to use from which to extract palm3d data.
    
    Args:
        loadfolder: Where to shortcut to for the user to select the palm3d
            data files.
    
    Returns:
        pklfiles: List of filenames where acquisition data (including drift)
            will be found.
        locsfiles: List of filenames for 'localization files', corresponding to
            pklfiles.
        
    """
    
    pklfiles = []
    locsfiles = []

    # Get localisations from palm3d data files

    print '\nPlease find me the first pickle (pkl)!'
    pklfiles.append(askopenfilename(initialdir=loadfolder))
    print '%s\n' % pklfiles

    print 'Please find me the localizations file.'
    print '(Usually in raw data folder, e.g. z=...)'
    print 'Use _palm_localizations for unlinked localisations'
    print 'or _palm_particles for localisations after linking.'
    locsfiles.append(askopenfilename(initialdir=loadfolder))
    print '%s' % locsfiles

    while(raw_input(
        '\nInclude another acquisition run from this FOV? [y]/n: '
        ) != 'n'):
        
        # Find the data, as before
        
        print '\nPlease find me that pickle then (pkl).'
        pklfiles.append(askopenfilename(initialdir=loadfolder))
        
        print 'And the relevant localizations file too.'
        locsfiles.append(askopenfilename(initialdir=loadfolder))
    
    return(pklfiles, locsfiles)
        
def getcorrecteddata(pkldata, locsfile, offset):
    """Get co-ordinates for selected data, corrected for drift.

    Args:
        pkldata: Aquisition data file (includes drift).
        locsfile: 'Localizations file' filename.
        offset: Drift of first frame relative to the first acquisition run for
            the FOV.

    Returns:
        locsdf: Resulting data as a pandas DataFrame.
    """

    # Separate out offset components for use when correcting x, y and z, below.    
    (xoffset, yoffset, zoffset) = offset

    # Get raw localisations (not drift-corrected).

    # locsimages is a shelve, or uber-list of images, called '0' '1', '2', etc.
    # Each image (locsimages['0'] or locsimages[repr(0)])
    # is a list of localisations within that image.
    # Each localisation (e.g. locsimages[repr(1)][1])
    # is a dictionary of properties.
    # Each localisation contains position, e.g. 'x', and other information.

    locsimages = shelve.open(locsfile, protocol=2)

    # Initialise co-ordinates and data quality/correlation arrays
    # Over all of the images, find drift-corrected coordinates and 'qual'
    xuncorr = []
    yuncorr = []
    zuncorr = []
    x = []
    y = []
    z = []
    qual = []
    counts = []
    imagenum = []
    abovebg = []
    numimages = []

    for imagecount in range(len(pkldata.images)):
        for loc in locsimages[repr(imagecount)]:
            # Can exclude poor correlations with calibration stack here
            # if(loc['qual'] > qualmin):
                # Can exclude the extremes in z
                # (where some incorrect localisations in z end up)
                # if(loc['z'] > 5 and loc['z'] < 75):
            xuncorr.append(loc['x'])
            yuncorr.append(loc['y'])
            zuncorr.append(loc['z'])
            x.append(loc['x'] - pkldata.drift['x'][imagecount]
                                  - xoffset)
            y.append(loc['y'] - pkldata.drift['y'][imagecount]
                                  - yoffset)
            z.append(loc['z'] - pkldata.drift['z'][imagecount]
                                  - zoffset)
            qual.append(loc['qual'])
            counts.append(loc['avg_counts'])
            imagenum.append(imagecount)
            if('avg_counts_above_bg' in loc):
                abovebg.append(loc['avg_counts_above_bg'])
                numimages.append(1 + len(loc['later_image_names']))
            else:
                abovebg.append(None)
                numimages.append(None)
    
    locsimages.close()
    
    
    # Pandas data frame
    locsdf = pd.DataFrame({'xuncorr': xuncorr, 'yuncorr': yuncorr,
                    'zuncorr': zuncorr,
                    'xpx': x, 'ypx': y, 'zpx': z,
                    'qual': qual, 'imagenum': imagenum, 'counts': counts,
                    'abovebg': abovebg, 'numimages': numimages})

    print '\n%i localisations found.' % locsdf.shape[0] # % len(locs)

    return locsdf

def combineacquisitions(pklfiles, locsfiles):        
    """Combine data from multiple acquisitions for analysis.
    User input required to find acquistion data.

    Args:
        Lists of filenames where palm3d data will be found.
        pklfiles: Files contain acquisition data (including drift).
        locsfiles: 'Localization files'.

    Returns:
        locsdf: Pandas DataFrame containing localisation data as in
            getcorrecteddata(), combined over the chosen acquisition repeats.
    """
    
    for i in range(len(pklfiles)):
        print('\nReading acqu\'n %i of %i. Please wait...' % (
                                                      (i+1) , len(pklfiles)))       
        # Get acquisition information, including drift.
        with open(pklfiles[i], 'rb') as f:
            pkldata = cPickle.load(f)
        
        # Localisations from first run - no offset
        if i == 0:
            veryfirstfidpos = pkldata.drift['initial_xyz']
        
            locsdf = getcorrecteddata(pkldata=pkldata, locsfile=locsfiles[i],
                          offset=np.array([0., 0., 0.]))
        
        # Localisations from subsequent runs, with offsets
        else:
            firstfidpos = np.array(pkldata.drift['initial_xyz'])
            offset = firstfidpos - veryfirstfidpos
            locs_next = getcorrecteddata(pkldata, locsfile=locsfiles[i],
                                         offset=offset)
            locsdf = locsdf.append(locs_next, ignore_index=True)
    
    return locsdf
    
def getconversions_pxtonm(): 
    """User input, returned as 3-item list to allow conversion."""
    x_to_nm = float(raw_input('\nHow many nm per pixel in x? '))
    y_to_nm = float(raw_input('How many nm per pixel in y? '))
    z_to_nm = float(raw_input('How many nm per pixel in z? '))
    xyz_conversion = (x_to_nm, y_to_nm, z_to_nm)
    return xyz_conversion        

def convert_pxtonm(locsdf, xyz_conversion): 
    """"Do pixel coordinate to nm conversion.        
    
    Args:
        locsdf: Localisations in pandas DataFrame, with xpx, ypx and zpx
            coordinates.
        xyz_conversion: x, y and conversion factors from px coordinates to nm.
        
    Returns:
        loscdf: The localisations pandas DataFrame, now including coordinates
            in nm.
    """
    locsdf['xnm'] = locsdf['xpx'] * xyz_conversion[0]
    locsdf['ynm'] = locsdf['ypx'] * xyz_conversion[1]
    locsdf['znm'] = locsdf['zpx'] * xyz_conversion[2]
    return locsdf

def countstophotons():
    """Get user input required to convert camera counts to photons.
    Returns:
        photoconversion: Dictionary containing this information
            (electronspercount, emgain, qe).
    """
    electronspercount = float(raw_input('How many electrons per count? '))
    emgain = float(raw_input('EM gain? '))
    qe = float(raw_input('QE (electrons per photon)? '))
    photonconversion = {'electronspercount': electronspercount,
                        'emgain': emgain, 'qe': qe}
    return photonconversion
    