# -*- coding: utf-8 -*-
"""palm3dextra.py

Generates a pandas dataframe of all drift-corrected localisations in a palm3d
acquisition, including rough precision estimate (S/sqrt(N)).

Also allows subsetting based on ROI, cross-correlation value with calibration
stack and precision estimate.

Requires on the PYTHONPATH:
    palm3d.py (https://github.com/AndrewGYork/palm3d)
    getlocsdata.py #
    usecaldata.py  #
    subsetlocs.py  # All provided alongside this script
    reconstruct.py #
    
Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""
import getlocsdata
import usecaldata
import subsetlocs
from reconstruct import refhistdf

import numpy as np
from Tkinter import Tk
from tkFileDialog import askdirectory
import cPickle

class Locsdata:
    """Localisation data management for palm3d output (York et al.,
    Nat Methods (2011), 8:327-333).
    """
    #def __init__(self, loadfolder, savefolder, linking, pklfiles, locsfiles,
    #             locs, xyz_conversion, calpkl, caldata,
    #             sdcalslices):
    def getname(self):
        self.name = raw_input('What is this dataset called? ')
                              
    def getfolders(self):
        print 'Where is the data we are going to handle (parent folder)?'
        self.loadfolder = askdirectory()
        print 'Where do you want to save the results?'
        self.savefolder = askdirectory(initialdir=self.loadfolder)
    
    # See York paper for linking (aka grouping)
    # of localisation across multiple frames.
    def getlinking(self):
        self.linking = raw_input(
                              'Are we using linked or unlinked locs [L/U]? ')
    
    # Localisations in px coordinates
    def getlocsbypx(self):
        self.pklfiles, self.locsfiles = getlocsdata.getp3dfiles(
                                                          self.loadfolder)
        
        self.locs = getlocsdata.combineacquisitions(
                                 self.pklfiles, self.locsfiles)
    
    # Localisation in nm coordinates
    def convertpxtonm(self):
        self.xyz_conversion = getlocsdata.getconversions_pxtonm()
        self.locs = getlocsdata.convert_pxtonm(
                                 self.locs, self.xyz_conversion)
    
    # Calibration data for the acquisition
    # will be kept with the localisation data.
    def getcaldata(self):
        self.calpkl, self.caldata = usecaldata.getcaldata(self.loadfolder)
    
    # Gaussian fit data for calibration images
    def getsdcalslices(self):
        self.sdcalslices = usecaldata.fitcal_twoD_Gaussian(self.caldata,
                                                           self.xyz_conversion)
    # Convert pixel values to number of photons.
    def convert_e_to_photons(self):
        self.conversiontophotons = getlocsdata.countstophotons()
    
    # Calculate number of photons above background for linked localisations
    # (see York paper).
    def getphotonsabovebg(self):
        # total photons
        # * pre-amp gain (from performance sheet) / EM gain / QE 
        # * area of slice * numslices
        # Don't need to subtract bias offset (-100) as this is 
        # cancelled out in subtraction of background images
        convfactor = (self.conversiontophotons['electronspercount'] / 
                     self.conversiontophotons['emgain'] / 
                     self.conversiontophotons['qe'])
        
        self.locs['photonsabovebg'] = (
                self.locs['abovebg'] * convfactor * 
                self.caldata.shape[0] * self.caldata.shape[1] * 
                self.locs['numimages'])
    
    # Estimate localisation precision - uses standard error for fitted Gaussian
    # (s.d. used is mean of two fitted axes).            
    def getprecision(self):
        self.locs['precisionnm'] = (
                self.sdcalslices[
                    self.locs.zuncorr.round().astype('int')] /
                np.sqrt(self.locs.photonsabovebg)
                )

# Construct a useful .pkl file containing localisations and associated data
def makedataset():
    d = Locsdata() # Set up palm3d localisation data management class
    # Populate instance variables of localisation data and save
    d.getname()
    d.getfolders()
    d.getlinking()
    d.getlocsbypx()
    d.convertpxtonm()
    d.getcaldata()
    if raw_input(
            'Do you want to convert to photons above background (y/[n])? '
            ) == 'y':
        d.convert_e_to_photons()
        d.getphotonsabovebg()
    if raw_input(
            'Do you want to calculate localisation precision in nm (y/[n])? '
            ) == 'y':
        d.getsdcalslices()
        d.getprecision()
    savefile = open(d.savefolder + '\\' + d.name + '.pkl', 'wb')
    cPickle.dump(d, savefile, protocol=2)
    d.refhist = refhistdf(d.locs.loc[
                              (d.locs.qual > 0.4)])
    savefile = open(d.savefolder + '\\' + d.name + '.pkl', 'wb')
    cPickle.dump(d, savefile, protocol=2)
    return d

# Make a subset of localisations from data contained in a Locsdata object,
# e.g. built using makedataset().
def subsetdata(d):
    df = d.locs # The localisations (not the acquisition settings, etc.)
    # Make the required subset and get parameters for use in filename
    subdf, params = subsetlocs.makesubset(df)
    # Save the subset
    if 'precmax' in params:
        savefile = d.savefolder + (
            '\\' + d.name +'_nm_hor%i-%i_vert%i-%i_z%i-%i_qualmin%.2f_maxprec%i.pkl'
            % (params['ymin'], params['ymax'],
              params['xmin'], params['xmax'], params['zmin'], params['zmax'],
                params['qualmin'], params['precmax'])
            )
        subdf.to_pickle(savefile)
    else:
        savefile = d.savefolder + (
            '\\' + d.name +'_nm_hor%i-%i_vert%i-%i_z%i-%i_qualmin%.2f.pkl'
            % (params['ymin'], params['ymax'],
              params['xmin'], params['xmax'], params['zmin'], params['zmax'],
                params['qualmin'])
            )
        subdf.to_pickle(savefile)
    return subdf

def main():
    d = makedataset()
    if raw_input(
"""Do you want to make a subset of the data
(ROI, correlation with calibration images,
localisation precision estimate)? (y/[n]) """) == 'y':
        subdf = subsetdata(d)        
        return d, subdf
    else:
        return d

if __name__ == '__main__':
    print('palm3dextra loaded.')
    if raw_input(
'Do you want to process palm3d acquisition data files now (y/[n])? ') == 'y':
        Tk().withdraw()
        main()
    print('\nHit Enter to exit')
    raw_input()
