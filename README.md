# palm3d_extra
This project provides data handling and flexibility for palm3d data.

**palm3d** is processing software by **Andrew York** (https://github.com/AndrewGYork/palm3d) to identify single molecule localisations in image sequences. It stores data on these localisations in its own complex format, which this software accesses and outputs in a convenient form.

This software was developed by **Alistair Curd** of the University of Leeds on 23 May 2018.

Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

# DEVELOPED WITH:
This software was developed in Python 2.7.17 and Anaconda on a Windows 7 system. The original *palm3d* script is not not yet ported to Python 3, and so this software currently remains in Python 2.

# USAGE:
In a Python 2.7 environment, run *palm3dextra.py* and follow instructions.

A suitable environment satisfying all requirements can be created like this:
1. Install Anaconda or Miniconda from anaconda.com
2. In an Anaconda prompt, navigate to the directory containing this software and type:
```conda env create -f environment_palm3d_extra.yml```

The environment can then be activated at any time by typing the following in an Anaconda prompt:
```conda activate palm3d_extra```

*palm3dextra.py* also requires *palm_3d.py* (https://github.com/AndrewGYork/palm3d) to run. e.g. put *palm_3d.py* in the same directory and run *palm3dextra.py* from that directory.

Usage of *palm3dextra.py* requires familiarity with the files generated as used by *palm3d*.

Running palm3dextra.py from the Windows Command Prompt may require running the following command beforehand, to avoid an encoding error:
```set PYTHONIOENCODING=utf-8```

This script will generate a pickle file containing acquisition data, including a pandas dataframe of all drift-corrected localisations in a palm3d acquisition, optionally with a rough precision estimate (S/sqrt(N)).

It also allows subsetting based on ROI, cross-correlation value with calibration
stack and precision estimate.

Functions in *reconstruct.py* can also be called from scripts, or used interactively, to generate binary files (.raw) containing image data. The files saved by these functions contain information for importing into *ImageJ* in the filename.

# FILES INCLUDED:
* README.md: This file, which contains information about the software in this project.
* environment_palm3d_extra.yml: Anaconda configuration file that creates the environment used to develop this software.
* license.md: Software licensing file.
* .gitignore: Hidden, configuration file for git, a distributed version-control system

## Python Code
* *palm3dextra.py*: A Python script which extracts localisation data from *palm3d* output files.
* *getlocsdata.py*: A Python module with functions for obtaining localisation coordinates, and asking for user input needed to estimate localisation precision.
* *usecaldata*: A Python module with functions to retrieve *palm3d* calibration data and fits 2D Gaussian distributions to it, in order to generate estimates of localisation precision.
* *subsetlocs.py*: A Python module with functions to filter localisations by ROI, quality (cross-correlation with calibration image) or precision estimate.
* *reconstruct.py*: Options for building histograms to display the localisations in an image.
