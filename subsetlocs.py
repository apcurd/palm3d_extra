# -*- coding: utf-8 -*-
"""subsetlocs.py

Filter localisation by ROI, localisation quality (cross-correlation with
calibration image) or precision estimate, if applicable).

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""
def makesubset(locs):
    """Subset data from a FOV to a smaller 3D ROI, with minimum
    cross-correlation against calibration stack and optionally maximum
    localisation precision estimate.
    
    Args:
        locs: A pandas dataframe containing drift-corrected localisations,
        columns are at least x, y, z, in raw data coordinates
        (e.g. camera pixels), qual (cross-correlation with calibration PSF).
        Precision estimate in nm is optional
        
    Returns:
        subset: Subset of locs filtered for max and min in x, y, z;
        above min of qual; below maximum set for precision estimate.
        
        params: Parameters used for the subsetting operation
    """    
    
    # y is horizontal pixel number in palm3d
    
    # Get subsetting parameters, apart from localisation precision
    # and store in dictionary
    print('\nTo subset a volume for analysis,')
    print('use pixel values from a reconstruction with original pixel size.')
    ymin = float(raw_input('\nMinimum horizontal pixel: '))
    ymax = float(raw_input('Maximum horizontal pixel: '))
    xmin = float(raw_input('Minimum vertical pixel: '))
    xmax = float(raw_input('Maximum vertical pixel: ')) 
    zmin = float(raw_input('Minimum depth pixel: '))
    zmax = float(raw_input('Maximum depth pixel: '))
    qualmin = float(raw_input(
          'Minimum correlation with calibration images: ')) 
    params = {'ymin': ymin, 'ymax': ymax, 'xmin': xmin, 'xmax': xmax,
              'zmin':zmin, 'zmax': zmax, 'qualmin': qualmin}
              
    # Do subsetting on dataframe    
    subset = locs.loc[(locs.ypx > ymin) & (locs.ypx < ymax) &
            (locs.xpx > xmin) & (locs.xpx < xmax) &
            (locs.zpx > zmin) & (locs.zpx < zmax) &
            (locs.qual > qualmin)]
    
    # Get precision parameter, add to dict, do further subset
    # if applicable and desired by user
    if 'precisionnm' in locs.columns:
        if raw_input('Do you want to include precision y/[n]? ') == 'y':
            precmax = float(raw_input('Maximum estimated precision (nm): '))
            subset = subset.loc[subset.precisionnm < precmax]
            params['precmax'] = precmax
    
    # Display number of localisations left within these parameters    
    print('\nSubset contains %i localisations.' % subset.shape[0])
    
    return(subset, params)
