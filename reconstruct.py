# -*- coding: utf-8 -*-
"""
Some options for building histograms.

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""
import numpy as np

def refhistdf(locs):
    sub = locs.loc[(locs.qual > 0.4)] # & (locs.precisionnm < 5)]
    edge = np.arange(350)
    hist,edges = np.histogramdd(
                     np.column_stack((sub.xpx, sub.ypx)), bins=(edge, edge))
    # hist is ready to do .tofile with
    return hist
   
def histogram3d(locs, savefolder):
    """
    Args:
        locs: Pandas dataframe of localisations, containing x, y, z in nm or
            pixels.
        y is horizontal in FOV.
    """
    coordchoice = raw_input("""Would you like to bin localisations by camera
                           pixels or by position in nm? (px / nm)""")
    
    if coordchoice == 'nm':
        binsize = float(raw_input('What binsize would you like to use (nm)? '))
        
        edgex = np.arange(locs.xnm.min(), locs.xnm.max(), binsize)
        edgey = np.arange(locs.ynm.min(), locs.ynm.max(), binsize)
        edgez = np.arange(locs.znm.min(), locs.znm.max(), binsize)
        hist, edges = np.histogramdd(np.column_stack(
                      (locs.xnm, locs.ynm, locs.znm)),
                      bins=(edgex, edgey, edgez))
        
        hist = hist.astype(np.uint32)
        # Hist will be written with last dimension varying first
        # So transpose so that last dimension will be horizontal pixel
        # when reading RAW with ImageJ ("y"),
        # then vertical ("x"), then depth ("z").
        hist = np.transpose(hist, (2,0,1))
        
        fname = '%s\\hist_%i-%i-%i_32uns.raw' % (
            savefolder,
            hist.shape[2], hist.shape[1], hist.shape[0])
     
        hist.tofile(fname)
        print 'Histogram saved as %s' % fname
        print ('Use the numbers in the file name for importing into ImageJ.')
    
    elif coordchoice == 'px':
        edgex = np.arange(0, locs.xpx.max() + 1)
        edgey = np.arange(0, locs.ypx.max() + 1)
        edgez = np.arange(0, locs.zpx.max() + 1)        
        hist, edges = np.histogramdd(np.column_stack(
                      (locs.xpx, locs.ypx, locs.zpx)),
                      bins=(edgex, edgey, edgez))

        hist = hist.astype(np.uint32)
        # Hist will be written with last dimension varying first
        # So transpose so that last dimension will be horizontal pixel
        # when reading RAW with ImageJ ("y"),
        # then vertical ("x"), then depth ("z").
        hist = np.transpose(hist, (2,0,1))
        
        fname = '%s\\hist_%i-%i-%i_32uns.raw' % (
            savefolder,
            hist.shape[2], hist.shape[1], hist.shape[0])
     
        hist.tofile(fname)
        print 'Histogram saved as %s' % fname
        print ('Use the numbers in the file name for importing into ImageJ.')

    
    else:
        hist = 'Sorry, no histogram. Please choose \'px\' or \'nm\' next time.'
        print 'Sorry, no histogram. Please choose \'px\' or \'nm\' next time.'
    
    return(hist)
    
def subhistogram(xyz, xmax, xmin, ymax, ymin, zmax, zmin,
                      conv_to_nm, savefolder, qualmin, linking):
    """Make the histogram.

    Args:
        Drift-corrected localisations and ranges.
        Conversion from pixel numbers to nm.
        The folder to save in.

    Returns:
        A histogram of localisation positions.
    """
    
    # Correct x, y, z to nm
    xmaxnm, ymaxnm, zmaxnm = np.multiply([xmax, ymax, zmax],
                                                 conv_to_nm)
    xminnm, yminnm, zminnm = np.multiply([xmin, ymin, zmin],
                                                 conv_to_nm)   
    
    binsize = float(raw_input('Histogram bin size (nm): '))

    # Bins are centred on integer numbers of binsize (1.5,
    # rather than 0.5 ensures extrema are beyond xmin and max)
    xminnm = int(xminnm / binsize) * binsize - 1.5 * binsize
    xmaxnm = int(xmaxnm / binsize) * binsize + 1.5 * binsize
    yminnm = int(yminnm / binsize) * binsize - 1.5 * binsize
    ymaxnm = int(ymaxnm / binsize) * binsize + 1.5 * binsize
    zminnm = int(zminnm / binsize) * binsize - 1.5 * binsize
    zmaxnm = int(zmaxnm / binsize) * binsize + 1.5 * binsize    

    edgex = np.arange(xminnm, xmaxnm, step=binsize)
    edgey = np.arange(yminnm, ymaxnm, step=binsize)
    edgez = np.arange(zminnm, zmaxnm, step=binsize)
    
    size = len(edgex) * len(edgey) * len(edgez) * 4 / 1000000
    print ('Histogram will be %i MB' % size)
    if (raw_input('Abort (y/[n])? ') == 'y'):
        return()   
    
    hist,edges = np.histogramdd(xyz, bins=(edgex, edgey, edgez))
    
    hist = hist.astype(np.uint32)
    # Hist will be written with last dimension varying first
    # So transpose so that last dimension will be horizontal pixel
    # when reading RAW with ImageJ ("y"),
    # then vertical ("x"), then depth ("z").
    hist = np.transpose(hist, (2,0,1))
    print ('Built')

    # Again, last dimension is changed first, so reverse histogram shape
    # to indicate sizes for importing into e.g. ImageJ
    fname = '%s\\hist_%inmbin_%i-%i-%i_32uns-x%i-%i_y%i-%i_z%i-%i_corr%i-%i-%i_qualmin%.2f_%s.raw' % (
        savefolder,
        int(binsize),
        hist.shape[2], hist.shape[1], hist.shape[0],
        xmin, xmax, ymin, ymax, zmin, zmax,
        conv_to_nm[0], conv_to_nm[1], conv_to_nm[2],
        qualmin, linking)
    hist.tofile(fname)
    print 'Histogram saved as %s' % fname

    return hist
    