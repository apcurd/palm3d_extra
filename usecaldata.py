# -*- coding: utf-8 -*-
"""usecaldata.py

Retrieves palm3d calibration data and fits 2D Gaussians to it, in order to
generate rough estimates of localisation precision with the widths of the
fitted surfaces.

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""
from tkFileDialog import askopenfilename
import cPickle
import numpy as np
import scipy.optimize as opt

def getcaldata(loadfolder):
    """
    Access palm3d calibration data
    
    Args:
        loadfolder: Folder location for the palm3d data
    
    Returns:
        calpkl: Filename of the .pkl calibration file
        caldata: The calibration data itself.
    """
    print '\nPlease find me the calibration pickle (pkl)!'
    calpkl = askopenfilename(initialdir=loadfolder)
    print '%s\n' % calpkl
    with open(calpkl, 'rb') as f:
        caldata = cPickle.load(f)
    return(calpkl, caldata)

def twoD_Gaussian((x, y), amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    """Describes 2D Gaussian function, including offset and rotation.
    Args:
        (x, y): 2D coordinates.
        Others: 2D Gaussian parameters.
    
    Returns:
        2D Gaussian function on ravelled coordinates.   
    """
    xo = float(xo)
    yo = float(yo)    
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                            + c*((y-yo)**2)))
    return g.ravel()

def fitcal_twoD_Gaussian(caldata, xyz_conversion):
    """Fit calibration images to a 2D Gaussian.
    Args:
        caldata: palm3d calibration data
        xyz_conversion: 3-item list for converting x, y, z pixel coordinates
            to nm.
            
    Returns: Array containing the mean width of the two axes of the fitted
        Gaussian, for each slice of the calibration stack
    """
    
    def twoD_Gaussian((x, y), amplitude, xo, yo, sigma_x, sigma_y,
                      theta, offset):
        """Describes 2D Gaussian function, including offset and rotation."""
        xo = float(xo)
        yo = float(yo)    
        a = (np.cos(theta)**2)/(2*sigma_x**2) + (
                                             np.sin(theta)**2)/(2*sigma_y**2)
        b = -(np.sin(2*theta))/(4*sigma_x**2) + (
                                             np.sin(2*theta))/(4*sigma_y**2)
        c = (np.sin(theta)**2)/(2*sigma_x**2) + (
                                             np.cos(theta)**2)/(2*sigma_y**2)
        g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                                + c*((y-yo)**2)))
        return g.ravel()    
    
    x, y = np.meshgrid(np.arange(caldata.shape[0]) * xyz_conversion[0],
                   np.arange(caldata.shape[1]) * xyz_conversion[1])
    sx = np.array([])
    sy = np.array([])
    smean = np.array([])
    
    for h in range(caldata.shape[2]):
        calslice = caldata[:, :, h]
        
        Amp_guess = np.max(calslice) - np.min(calslice)
        x0_guess = np.max(x) / 2.
        y0_guess = np.max(y) / 2.
        sx_guess = np.max(x) / 4.
        sy_guess = np.max(y) / 4.
        theta_guess = 0.
        offset_guess = np.min(calslice)
        
        popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), np.ravel(calslice.T),
                                   p0=(Amp_guess, x0_guess, y0_guess,
                                       sx_guess, sy_guess, theta_guess,
                                       offset_guess))
        
        sx = np.append(sx, popt[3])
        sy = np.append(sy, popt[4])
        smean = np.append(smean, (popt[3] + popt[4]) / 2)
    
    return smean
